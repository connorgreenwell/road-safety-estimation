import analysis_util
import dataset
import definition
import numpy as np
import tensorflow as tf

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

train_csv_split = '../model/dataset/crossview_train_split.csv'
test_csv_split = '../model/dataset/crossview_test_split.csv'
    
def getRandomData():
    _, bin_train, srs_train, _, _, _ = dataset.load_train(train_csv_split)
    _, bin_test, srs_test, _, _, _ = dataset.load_test(test_csv_split)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())

        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(coord=coord)

        srs_errors = []
        bin_errors = []

        batch_num = 0

        try:
            while(True):
                print(batch_num)
                batch_num += 1

                b_train, s_train, b_test, s_test = sess.run([
                    bin_train, srs_train, bin_test, srs_test])

                s_train = s_train[:len(s_test)]
                b_train = b_train[:len(b_test)]

                srs_error = np.abs(s_train - s_test)
                bin_error = np.abs(b_train - b_test)

                srs_errors.extend(srs_error)
                bin_errors.extend(bin_error)
        
        except tf.errors.OutOfRangeError:
            coord.request_stop()
            coord.join(threads)

            random = {}

            srs_errors = sorted(srs_errors)
            srs_errors = [ x for x in srs_errors if x < 30 ]
            srs_y_data = np.arange(1, len(srs_errors)+1, 1, dtype=np.float32)
            srs_y_data /= len(srs_errors)
            random['srs_errors'] = srs_errors
            random['srs_y_data'] = srs_y_data
            random['srs_auc'] = analysis_util.getAUC(srs_errors, srs_y_data)

            bin_errors = sorted(bin_errors)
            bin_y_data = np.arange(1, len(bin_errors)+1, 1, dtype=np.float32)
            bin_y_data /= len(bin_errors)
            random['bin_errors'] = bin_errors
            random['bin_y_data'] = bin_y_data
            random['bin_auc'] = analysis_util.getAUC(bin_errors, bin_y_data)
            return random
