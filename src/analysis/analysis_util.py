def getAUC(x, y):
    auc = 0.0
    points = zip(x,y)
    for idx in range(1, len(x)):
        x_0, y_0 = points[idx-1]
        x_1, y_1 = points[idx]
        auc += (x_1-x_0) * (y_1+y_0) / 2

    return auc / max(x)
