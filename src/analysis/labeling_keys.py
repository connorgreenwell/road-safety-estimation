lookup = {}

lookup['area_type'] = {
    1 : 'Urban',
    2 : 'Rural',
}

lookup['intersect_channel'] = {
    1 : 'Not present',
    2 : 'Present',
}

lookup['curvature'] = {
    1 : 'Straight/gentle Curve',
    2 : 'Moderate',
    3 : 'Sharp',
    4 : 'Very sharp',
}

lookup['upgrade_cost'] = {
    1 : 'Low',
    2 : 'Medium',
    3 : 'High',
}

lookup['motorcycle_flow'] = {
    1 : 'None',
    2 : '1',
    3 : '2-3',
    4 : '4-5',
    5 : '6-7',
    6 : '8+',
}

lookup['bicycle_flow'] = {
    1 : 'None',
    2 : '1',
    3 : '2-3',
    4 : '4-5',
    5 : '6-7',
    6 : '8+',
}

lookup['pedestrian_flow'] = {
    1 : 'None',
    2 : '1',
    3 : '2-3',
    4 : '4-5',
    5 : '6-7',
    6 : '8+',
}

lookup['drive_side_lane_use'] = {
    1 : 'Undeveloped areas',
    2 : 'Farming and agricultural',
    3 : 'Residential',
    4 : 'Commercial',
    5 : 'Not recorded',
    6 : 'Educational',
    7 : 'Industrial and manufacturing',
}

lookup['passen_side_lane_use'] = {
    1 : 'Undeveloped areas',
    2 : 'Farming and agricultural',
    3 : 'Residential',
    4 : 'Commercial',
    5 : 'Not recorded',
    6 : 'Educational',
    7 : 'Industrial and manufacturing',
}

lookup['median_type'] = {
    1  : 'Safety barrier - metal',
    2  : 'Safety barrier - concrete',
    3  : 'Physical median width >= 20m',
    4  : 'Physical median width 10 to < 20m',
    5  : 'Physical median width 5 to < 10m',
    6  : 'Physical median width 1 to < 5m',
    7  : 'Physical median width 0 to < 1m',
    8  : 'Continuous central turning lane',
    9  : 'Flexible posts',
    10 : 'Central hatching (>1m)',
    11 : 'Centre line',
    12 : 'Safety barrier - motorcycle friendly',
    13 : 'One wayl',
    14 : 'Wide centre line (0.3m to 1m)',
    15 : 'Safety barrier - wire rope',
}

lookup['centre_rumble_strip'] = {
    1  : 'Not present',
    2  : 'Present',
}

lookup['roadside_driver_side_distance'] = {
    1  : '0 to <1m',
    2  : '1 to <5m',
    3  : '5 to <10m',
    4  : '>=10m',
}

lookup['roadside_driver_side_object'] = {
    1  : 'Safety barrier - metal',
    2  : 'Safety barrier - concrete',
    3  : 'Safety barrier - motorcycle friendly',
    4  : 'Safety barrier - wire rope',
    5  : 'Aggressive vertical face',
    6  : 'Upwards slope - (15 to 75 degrees)',
    7  : 'Upwards slope - (>= 75 degrees)',
    8  : 'Deep drainage ditch',
    9  : 'Downwards slope (> -15 degrees)',
    10 : 'Cliff',
    11 : 'Tree >= 10cm',
    12 : 'Non-frangible sign/post/pole >= 10cm',
    13 : 'Non-frangible structure/bridge or building',
    14 : 'Frangible structure or building',
    15 : 'Unprotected safety barrier end',
    16 : 'Large boulders >= 20cm high',
    17 : 'No object',
}

lookup['roadside_passenger_side_distance'] = {
    1  : '0 to <1m',
    2  : '1 to <5m',
    3  : '5 to <10m',
    4  : '>=10m',
}

lookup['roadside_passenger_side_object'] = {
    1  : 'Safety barrier - metal',
    2  : 'Safety barrier - concrete',
    3  : 'Safety barrier - motorcycle friendly',
    4  : 'Safety barrier - wire rope',
    5  : 'Aggressive vertical face',
    6  : 'Upwards slope - (15 to 75 degrees)',
    7  : 'Upwards slope - (>= 75 degrees)',
    8  : 'Deep drainage ditch',
    9  : 'Downwards slope (> -15 degrees)',
    10 : 'Cliff',
    11 : 'Tree >= 10cm',
    12 : 'Non-frangible sign/post/pole >= 10cm',
    13 : 'Non-frangible structure/bridge or building',
    14 : 'Frangible structure or building',
    15 : 'Unprotected safety barrier end',
    16 : 'Large boulders >= 20cm high',
    17 : 'No object',
}

lookup['shoulder_rumble_strip'] = {
    1  : 'Not present',
    2  : 'Present',
}

lookup['driver_side_paved_shoulder'] = {
    1  : 'Wide (>= 2.4m)',
    2  : 'Medium (>= 1.0m to < 2.4m)',
    3  : 'Narrow (?= 0m to < 1.0m',
    4  : 'None',
}

lookup['passenger_side_paved_shoulder'] = {
    1  : 'Wide (>= 2.4m)',
    2  : 'Medium (>= 1.0m to < 2.4m)',
    3  : 'Narrow (?= 0m to < 1.0m',
    4  : 'None',
}

lookup['intersection_road_volume'] = {
    1  : '>= 15,000 vehicles',
    2  : '10,000 to 15,000 vehicles',
    3  : '5,000 to 10,000 vehicles',
    4  : '1,000 to 5,000 vehicles',
    5  : '100 to 1,000 vehicles',
    6  : '1 to 100 vehicles',
    7  : 'Not applicable',
}

lookup['intersection_quality'] = {
    1  : 'Adequate',
    2  : 'Poor',
    3  : 'Not applicable',
}

lookup['num_lanes'] = {
    1  : 'One',
    2  : 'Two',
    3  : 'Three',
    4  : 'Four or more',
    5  : 'Two and one',
    6  : 'Three and two',
}

lookup['lane_width'] = {
    1  : 'Wide (>= 3.25m)',
    2  : 'Medium (>= 2.75m to < 3.25m)',
    3  : 'Narrow (>= 0m to < 2.75)',
}

lookup['curve_quality'] = {
    1  : 'Adequate',
    2  : 'Poor',
    3  : 'Not aplicable',
}

lookup['road_condition'] = {
    1  : 'Good',
    2  : 'Medium',
    3  : 'Poor',
}

lookup['vehicle_parking'] = {
    1  : 'Low',
    2  : 'Medium',
    3  : 'High',
}

lookup['passenger_side_sidewalk'] = {
    1  : 'Physical barrier',
    2  : 'Non-physical separation >= 3.0m',
    3  : 'Non-physical separation 1.0m to < 3.0m',
    4  : 'Non-physical separation 0m to < 1.0m',
    5  : 'None',
    6  : 'Informal path >= 1.0m',
    7  : 'Informal path 0m to < 1.0m',
}

lookup['driver_side_sidewalk'] = {
    1  : 'Physical barrier',
    2  : 'Non-physical separation >= 3.0m',
    3  : 'Non-physical separation 1.0m to < 3.0m',
    4  : 'Non-physical separation 0m to < 1.0m',
    5  : 'None',
    6  : 'Informal path >= 1.0m',
    7  : 'Informal path 0m to < 1.0m',
}

lookup['motorcycle_facilities'] = {
    1  : 'Exclusive one way motorcycle path with barrier',
    2  : 'Exclusive one way motorcycle path without barrier',
    3  : 'Exclusive two way motorcycle path with barrier',
    4  : 'Exclusive two way motorcycle path without barrier',
    5  : 'Inclusive motorcycle lane on roadway',
    6  : 'None',
}

lookup['bicycle_facilities'] = {
    1  : 'Off-road path with barrier',
    2  : 'Off-road path',
    3  : 'On-road lane',
    4  : 'None',
    5  : 'Extra wide outside (>= 4.2m)',
    6  : 'Signed shared roadway',
    7  : 'Shared use path',
}
