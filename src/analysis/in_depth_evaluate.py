import sys
sys.path.append('../model/')
 
import labeling_keys
import util
import random
import definition
import numpy as np
import pandas as pd
import random_check
import analysis_util
from PIL import Image

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

def to_float(x):
    return np.array(x, dtype=np.float32)

args = util.parse_arguments()
run_name = util.run_name(args)

img_base = definition.img_base
csv_base = '../model/dataset/'
spec = definition.spec

checkpoint_dir = definition.ckpt_base + run_name + '/'
errors_file = checkpoint_dir + 'errors.csv'
predicts_file = checkpoint_dir + 'predicts.csv'
grounds_file = checkpoint_dir + 'grounds.csv'

# Load all csv files
errors = pd.read_csv(errors_file, dtype=str)
predicts = pd.read_csv(predicts_file, dtype=str)
grounds = pd.read_csv(grounds_file, dtype=str)

# Find max predict
max_predict_idx = np.argmax(to_float(predicts['srs_score']))
predict_lat = predicts['latitude'][max_predict_idx]
predict_lon = predicts['longitude'][max_predict_idx]
max_predict = predict_lat + ',' + predict_lon
max_predict_fname = img_base + max_predict + '/pano_cut.jpg'

with open(csv_base + 'crossview_train_split.csv', 'r') as f:
    train_lat = ''
    train_lon = ''
    max_train = 0.0

    for line in f:
        tokens = line.strip().split(',')
        if max_train < float(tokens[2]):
            train_lat, train_lon = tokens[0], tokens[1]
            max_train = float(tokens[2])

with open(csv_base + 'crossview_test_split.csv', 'r') as f:
    test_lat = ''
    test_lon = ''
    max_test = 0.0

    for line in f:
        tokens = line.strip().split(',')
        if max_test < float(tokens[2]):
            test_lat, test_lon = tokens[0], tokens[1]
            max_test = float(tokens[2])

max_train_fname = img_base + train_lat + ',' + train_lon + '/pano_cut.jpg'
max_test_fname = img_base + test_lat + ',' + test_lon + '/pano_cut.jpg'

print('Max Prediction: ' + max_predict_fname)
print('Max Train: ' + max_train_fname)
print('Max Test: ' + max_test_fname)

max_predict_img = Image.open(max_predict_fname)
max_train_img = Image.open(max_train_fname)
max_test_img = Image.open(max_test_fname)

def img_and_details(lat, lon, img, title):
    ground_idx = np.where(grounds['latitude'] == lat) \
                    and np.where(grounds['longitude'] == lon)
    predict_idx = np.where(predicts['latitude'] == lat) \
                    and np.where(predicts['longitude'] == lon)

    ground_srs = grounds['srs_score'][ground_idx[0][0]]
    ground_bin = grounds['srs_bins'][ground_idx[0][0]]
    predict_srs = predicts['srs_score'][predict_idx[0][0]]
    predict_bin = predicts['srs_bins'][predict_idx[0][0]]

    description = []
    description.append('Ground SRS Score : %s' % ground_srs)
    description.append('Ground SRS Bin   : %s' % ground_bin)
    description.append('Predict SRS Score: %s' % predict_srs)
    description.append('Predict SRS Bin  : %s' % predict_bin)

    plt.imshow(img)
    plt.plot(0,0)
    plt.plot(0,0)
    plt.plot(0,0)
    plt.plot(0,0)
    plt.axis('off')
    plt.legend(description, loc='best', fontsize='xx-small')
    plt.savefig(checkpoint_dir + title + '.png')
    plt.clf()

img_and_details(predict_lat, predict_lon, max_predict_img, 'max_predict')
# img_and_details(train_lat, train_lon, max_train_img, 'max_train')
img_and_details(test_lat, test_lon, max_test_img, 'max_test')

srs_errors = sorted(to_float(errors['srs_score']))
srs_errors = [ x for x in srs_errors if x < 30 ]
srs_y = np.arange(1, len(srs_errors)+1, 1, dtype=np.float32) / len(srs_errors)
srs_auc = analysis_util.getAUC(srs_errors, srs_y)

bin_errors = sorted(to_float(errors['srs_bins']))
bin_y = np.arange(1, len(bin_errors)+1, 1, dtype=np.float32) / len(bin_errors)
bin_auc = analysis_util.getAUC(bin_errors, bin_y)

random_data = random_check.getRandomData()

plt.plot(srs_errors, srs_y, label='Predict Score ROC: %.2f' % srs_auc) 
plt.plot(random_data['srs_errors'], random_data['srs_y_data'],
         label='Random Score ROC: %.2f' % random_data['srs_auc'])
plt.plot(bin_errors, bin_y, label='Predict Bin ROC: %.2f' % bin_auc) 
plt.plot(random_data['bin_errors'], random_data['bin_y_data'],
         label='Random Bin ROC: %.2f' % random_data['bin_auc'])
plt.legend(loc=4)
plt.savefig(checkpoint_dir + 'srs_roc.png')
plt.clf()

fig = plt.figure()
fig.suptitle('SRS Error Box Plots')

ax1 = fig.add_subplot(121)
ax1.set_title('SRS Score Error')
ax1.boxplot(to_float(errors['srs_score']))

ax2 = fig.add_subplot(122)
ax2.set_title('SRS Bin Error')
ax2.boxplot(to_float(errors['srs_bins']))

fig.subplots_adjust(top=0.85)
fig.savefig(checkpoint_dir + 'srs_box_plot.png')
plt.clf()

# Bar Chart for wrong predictions of aux labels
aux_errors = []

for key in spec.keys():
    aux_errors.append(
            (np.count_nonzero(to_float(errors[key]))/float(len(errors)),key))

aux_errors.sort()
aux_num_errors = [x[0] for x in aux_errors]
aux_keys = [x[1] for x in aux_errors]

plt.bar(range(len(aux_keys)), aux_num_errors)
plt.xticks(range(len(aux_keys)), aux_keys, rotation='vertical')
plt.margins(0.1)
plt.subplots_adjust(bottom=0.1)
plt.ylabel('Percentage of Wrong Predicts')
plt.title('Auxiliary Label Wrong Predicts')
plt.tight_layout()
plt.savefig(checkpoint_dir + 'aux_wrong_predicts.png')
plt.clf()

num_examples_to_show = 10

label_keys = labeling_keys.lookup

for img_idx in range(num_examples_to_show):
    idx = int(random.random() * len(grounds))
    lat, lon = predicts['latitude'][idx], predicts['longitude'][idx]
    pano_path = img_base + lat + ',' + lon + '/pano_cut.jpg'
    sat_path = img_base + lat + ',' + lon + '/satellite.jpg'


    description = []
    for key in spec.keys():
        if label_keys.keys().count(key):
            description.append('%s: %s' 
                    % (key, label_keys[key][int(grounds[key][idx])+1]))
        else:
            description.append('%s: %d' % (key, int(grounds[key][idx])+1))

    plt.imshow(Image.open(pano_path))
    for _ in range(len(spec.keys())):
        plt.plot(0,0)
    plt.axis('off')
    leg = plt.legend(description, loc='center left', fontsize='xx-small',
                     bbox_to_anchor=(1,0.5))
    plt.savefig(checkpoint_dir + 'sample_%02d.png' % img_idx, 
                bbox_extra_artists=(leg,),
                bbox_inches='tight',
                dpi=300)
    plt.clf()
