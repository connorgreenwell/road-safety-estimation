import dataset
import tensorflow as tf
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from datetime import datetime
from scipy.misc import imresize

if __name__ == '__main__':
    feat, srs_bins, lat, lon = dataset.load_train()

    with tf.Session(config=tf.ConfigProto()) as sess:
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())

        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(coord=coord)

        for idx in range(10):
            tstart = datetime.now()
            _feat, _srs_bins, _lat, _lon = sess.run(
                    [feat, srs_bins, lat, lon])
            tend = datetime.now()

            print(tend-tstart)

            for idx in range(len(_lat)):
                print('%s | %s | %d' % 
                    (_lat[idx], _lon[idx], _srs_bins[idx]))
            
            fig = plt.figure(1)

            for idx in range(1,10):
                ax = plt.subplot(3, 3, idx)
                plt.axis('off')
                img = imresize(_feat[idx], (70, 70, 3))
                plt.imshow(img)

            plt.tight_layout()
            plt.show()

        coord.request_stop()
        coord.join(threads)

