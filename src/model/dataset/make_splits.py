import csv
import glob
import bin_utils
import numpy as np
import pandas as pd
import cPickle as pickle
from random import shuffle
from collections import OrderedDict
from sklearn.model_selection import train_test_split

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import sys
sys.path.append('../')
import definition

def as_float(array):
    return np.array(array, dtype=np.float32)

def as_int(array):
    return np.array(array, dtype=np.int32)

csv_base  = definition.raw_csv_base 

csv_list = glob.glob(csv_base + '*.csv')

# Specification of auxiliary labels we want to predict
spec = definition.spec

train = OrderedDict()
test = OrderedDict()
num_bins = {}

train['Latitude'] = []
train['Longitude'] = []
train['srs_score'] = []
train['srs_score_bins'] = []

test['Latitude'] = []
test['Longitude'] = []
test['srs_score'] = []
test['srs_score_bins'] = []

# Pre-add all keys into both train and test dictionaries
# We need them to be in specific order to read them later on
# Data that needs binning will have their binned values right after
for key in spec.keys():
    train[key] = []
    test[key] = []

    if spec[key]['needs_binning']:
        train[key + '_bins'] = []
        test[key + '_bins'] = []
    
for csv_file in csv_list:
    df = pd.read_csv(csv_file, dtype=str)

    # Split dataset
    train_split, test_split = train_test_split(df, test_size=0.2)

    # Add lat, lon, and srs_score first, since we always need those
    train['Latitude'].extend(train_split['Latitude'])
    train['Longitude'].extend(train_split['Longitude'])
    test['Latitude'].extend(test_split['Latitude'])
    test['Longitude'].extend(test_split['Longitude'])
    train['srs_score'].extend(as_float(train_split['Vehicle SRS Total']))
    test['srs_score'].extend(as_float(test_split['Vehicle SRS Total']))

    # Then we add auxiliary labels
    for key in spec.keys():
        train[key].extend(as_int(train_split[spec[key]['csv_index']]) - 1)
        test[key].extend(as_int(test_split[spec[key]['csv_index']]) - 1)

# We know we need to bin srs_scores, so we do it separately
all_scores = train['srs_score'] + test['srs_score']
srs_edges = bin_utils.computeBin(
        all_scores, definition.srs_score_desired_num_bins)

# Save distribution of data as plots
plt.plot(range(len(all_scores)), sorted(all_scores))
plt.title('Raw Score')
plt.tight_layout()
plt.savefig('./raw_score_distribution.png')
plt.clf()

dist, _ = np.histogram(all_scores, srs_edges)
plt.bar(range(len(dist)), dist)
plt.title('Num Items in Bins')
plt.tight_layout()
plt.savefig('./items_in_bins_distribution.png')

train['srs_score_bins'] = np.digitize(train['srs_score'], srs_edges) - 1
test['srs_score_bins'] = np.digitize(test['srs_score'], srs_edges) - 1

# Need to have number of bins as a single-element array, to convert
# dict to panda data frame later on
num_bins['srs_score'] = [len(srs_edges),]
with open('./srs_score_bins.pkl', 'wb') as pf:
    pickle.dump(srs_edges, pf)

# Then we bin auxiliary labels
for key in spec.keys():
    if spec[key]['needs_binning']:
        all_values = train[key] + test[key]
        edges = bin_utils.computeBin(all_values, spec[key]['num_classes'])
        train[key + '_bins'] = np.digitize(train[key], edges) - 1
        test[key + '_bins'] = np.digitize(test[key], edges) - 1
        # Again, number of bins have to be a single-element array
        num_bins[key] = [len(edges),]

        # Store our edges to be used in the future
        with open(key + '_bins.pkl', 'wb') as pf:
            pickle.dump(edges, pf)

    else:
        # Again, number of bins have to be a single-element array
        num_unique_values = len(np.unique(train[key] + test[key]))
        # Check to make sure number of unique values in the coding manual
        # is what we need for classification
        if num_unique_values <= spec[key]['num_classes']:
            num_bins[key] = [spec[key]['num_classes'],]
        else:
            print('Not enough bins for %s, should be %d, but got %d'
                    % (key, spec[key]['num_classes'], num_unique_values))

# Turn below dictionaries into python lists
train_list = [train[key] for key in train.keys()]
test_list = [test[key] for key in test.keys()]

train_list = zip(*train_list)
test_list = zip(*test_list)

# Shuffle training data
shuffle(train_list)

# Convert train test lists into panda dataframe for easy csv conversion
train_df = pd.DataFrame(data=train_list)
test_df = pd.DataFrame(data=test_list)
train_df.to_csv('train_split.csv', index=False, header=False)
test_df.to_csv('test_split.csv', index=False, header=False)

# Save the number of bins for each label that needs binning
num_bins_df = pd.DataFrame.from_dict(num_bins)
num_bins_df.to_csv('num_bins.csv', index=False, header=True)
