import os

import sys
sys.path.append('../')

import definition

img_base = definition.img_base

def remove_invalid_data(split_fname):
    f = open(split_fname, 'r')
    f_lines = f.readlines()
    f.close()

    num_missing = 0

    with open('crossview_' + split_fname, 'w') as f:
        for line in f_lines:
            lat, lon = line.split(',')[0], line.split(',')[1]
            pano_cut_fname = img_base + lat + ',' + lon + '/pano_cut.jpg'
            if os.path.exists(pano_cut_fname):
                f.write(line)
            else:
                print('%s,%s pano not found, skipping' % (lat, lon))
                num_missing += 1

    print('For %s, %d number of panos are missing'%(split_fname, num_missing))
    return num_missing

if __name__ == '__main__':
    total_num_missing = 0

    total_num_missing += remove_invalid_data('train_split.csv')
    total_num_missing += remove_invalid_data('test_split.csv')

    print('Totally, %d number of panos are missing' % total_num_missing)
