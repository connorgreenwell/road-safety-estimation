import numpy as np
import pandas as pd
import cPickle as pickle

def computeBin(values, desired_num_bins):
    values = np.unique(values)
    
    edge_idx = np.arange(0, len(values)-1, len(values) / (desired_num_bins-1))

    edges = [values[i] for i in edge_idx]

    # while len(edges) > (desired_num_bins-1):
    #     min_diff = 1000
    #     min_idx  = 0
    #     for idx in range(1, len(edges)):
    #         if min_diff > (edges[idx] - edges[idx-1]):
    #             min_diff = edges[idx] - edges[idx-1]
    #             min_idx = idx
    #     removed.append(edges[idx])
    #     del edges[idx]

    edges.insert(0, -float('Inf'))
    edges.append(float('Inf'))

    return edges

def uniqueBin(values):
    values = np.unique(values)

    np.insert(values, 0, -float('Inf'))
    np.append(values, float('Inf'))

    print len(values)-1

    pickle.dump(values, open('./unique_edges.pkl', 'w'))

    return values, len(values)-1
