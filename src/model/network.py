from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import util
import tensorflow as tf 
import tensorflow.contrib.slim.nets
import tensorflow.contrib.slim as slim

inception = tf.contrib.slim.nets.inception

args = util.parse_arguments()

def vgg16(inputs, num_classes):
    with slim.arg_scope([slim.conv2d, slim.fully_connected],
            activation_fn=tf.nn.relu,
            weights_initializer=tf.truncated_normal_initializer(0.0, 0.01),
            weights_regularizer=slim.l2_regularizer(0.0005)):
        net = slim.repeat(inputs, 2, slim.conv2d, 64, [3, 3], scope='conv1')
        net = slim.max_pool2d(net, [2, 2], scope='pool1')
        net = slim.repeat(net, 2, slim.conv2d, 128, [3, 3], scope='conv2')
        net = slim.max_pool2d(net, [2, 2], scope='pool2')
        net = slim.repeat(net, 3, slim.conv2d, 256, [3, 3], scope='conv3')
        net = slim.max_pool2d(net, [2, 2], scope='pool3')
        net = slim.repeat(net, 3, slim.conv2d, 512, [3, 3], scope='conv4')
        net = slim.max_pool2d(net, [2, 2], scope='pool4')
        net = slim.repeat(net, 3, slim.conv2d, 512, [3, 3], scope='conv5')
        net = slim.max_pool2d(net, [2, 2], scope='pool5')
        net = slim.flatten(net)
        net = slim.fully_connected(net, 4096, scope='fc6_ignore')
        net = slim.dropout(net, 0.5, scope='dropout6_ignore')
        net = slim.fully_connected(net, 4096, scope='fc7_ignore')
        net = slim.dropout(net, 0.5, scope='dropout7_ignore')
        net = slim.fully_connected(net, num_classes, 
                activation_fn=None, scope='fc8_ignore')
        net = tf.nn.softmax(net)

    return net

def inception_v3(inputs, num_classes):
    with slim.arg_scope(inception_arg_scope()):
        net, end_points = inception.inception_v3(
                                            inputs,
                                            depth_multiplier=args.capacity,
                                            num_classes=num_classes,
                                            is_training=True)
    return net

def network(inputs, num_classes):
    if args.network == 'vgg16':
        print('Using VGG16 network')
        return vgg16(inputs, num_classes)
    if args.network == 'inception3':
        print('Using Inception V3 network')
        return inception_v3(inputs, num_classes)
    
# Taken from Google Slim Code
def inception_arg_scope(weight_decay=0.0005,
                        use_batch_norm=True,
                        batch_norm_decay=0.97,
                        batch_norm_epsilon=0.001):
  """Defines the default arg scope for inception models.

  Args:
    weight_decay: The weight decay to use for regularizing the model.
    use_batch_norm: "If `True`, batch_norm is applied after each convolution.
    batch_norm_decay: Decay for batch norm moving average.
    batch_norm_epsilon: Small float added to variance to avoid dividing by zero
      in batch norm.

  Returns:
    An `arg_scope` to use for the inception models.
  """
  batch_norm_params = {
      # Decay for the moving averages.
      'decay': batch_norm_decay,
      # epsilon to prevent 0s in variance.
      'epsilon': batch_norm_epsilon,
      # collection containing update_ops.
      'updates_collections': tf.GraphKeys.UPDATE_OPS,
  }
  if use_batch_norm:
    normalizer_fn = slim.batch_norm
    normalizer_params = batch_norm_params
  else:
    normalizer_fn = None
    normalizer_params = {}
  # Set weight_decay for weights in Conv and FC layers.
  with slim.arg_scope([slim.conv2d, slim.fully_connected],
                      weights_regularizer=slim.l2_regularizer(weight_decay)):
    with slim.arg_scope(
        [slim.conv2d],
        weights_initializer=slim.variance_scaling_initializer(),
        activation_fn=tf.nn.relu,
        normalizer_fn=normalizer_fn,
        normalizer_params=normalizer_params) as sc:
      return sc
