import tensorflow as tf
import numpy as np
import os
import glob
import random
import util
import definition

args = util.parse_arguments()

data_base = definition.data_base 
img_base  = definition.img_base
csv_base  = definition.csv_base

def process_img(lat, lon, what_to_grab, is_training=True):
    fname = img_base + lat + ',' + lon + '/%s.jpg' % what_to_grab
    image_data = tf.read_file(fname)
    image_data = tf.image.decode_jpeg(image_data, channels=3)
    
    if is_training and what_to_grab == 'satellite':
        offset_height = int(random.random() * (325-299))
        offset_width = int(random.random() * (325-299))
        image_data = tf.image.crop_to_bounding_box(image_data,
                                                   offset_height,
                                                   offset_width,
                                                   299,
                                                   299)
        image_data = tf.image.random_flip_up_down(image_data)
        image_data = tf.image.random_flip_left_right(image_data)

    else:
        image_data = tf.image.resize_images(image_data, tf.constant([299,299]))

    image_data = tf.cast(image_data, tf.float32)
    image_data /= 255

    return image_data

def load_csv(fname, is_training=True, num_epochs=None):

    fq = tf.train.string_input_producer([fname], num_epochs=num_epochs) 
    reader = tf.TextLineReader()
    key, value = reader.read(fq)
    data = tf.decode_csv(value,record_defaults=definition.defaults)

    lat, lon = data[0], data[1]
    srs_score = data[2]
    srs_bins = tf.to_int32(data[3])
    labels = data[4:]    

    if args.input == 'aerial':
        aerial_img = process_img(lat,lon,'satellite',is_training=is_training)
        return aerial_img, srs_bins, srs_score, labels, lat, lon 

    if args.input == 'ground':
        ground_img = process_img(lat,lon,'pano_cut',is_training=is_training)
        return ground_img, srs_bins, srs_score, labels, lat, lon 

    if args.input == 'crossview':
        aerial_img = process_img(lat,lon,'satellite',is_training=is_training)
        ground_img = process_img(lat,lon,'pano_cut',is_training=is_training)
        return aerial_img, ground_img, srs_bins, srs_score, labels, lat, lon 

def load_train(csv_file=None):
    if csv_file == None:
        data = load_csv('./dataset/crossview_train_split.csv',is_training=True)
    else:
        data = load_csv(csv_file, is_training=True)

    tensors = tf.train.shuffle_batch(data,
                                     num_threads=4,
                                     capacity=1000,
                                     min_after_dequeue=500,
                                     batch_size=32)

    return tensors

def load_test(csv_file=None):
    if csv_file == None:
        data = load_csv('./dataset/crossview_test_split.csv',
                        is_training=False,
                        num_epochs=1)
    else:
        data = load_csv(csv_file, is_training=False, num_epochs=1)

    tensors = tf.train.batch(data,
                             batch_size=32,
                             num_threads=4,
                             capacity=1000,
                             allow_smaller_final_batch=True)

    return tensors
