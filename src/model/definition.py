import os
import util
from collections import OrderedDict

args = util.parse_arguments()

if args.gpu01:
    data_base = '/home/wso226/data/road-safety-prediction/'
else:
    data_base = '/u/vul-d1/scratch/wso226/road-safety-estimation/'

img_base = data_base + 'images/'
csv_base = os.path.abspath('./dataset/') + '/'
raw_csv_base = data_base + 'csv/'
inception_v3_ckpt = data_base + 'inception_v3.ckpt'
places_ckpt = data_base + 'vgg16/vgg16.ckpt'
ckpt_base = data_base + 'checkpoints/'
log_base = data_base + 'logs/'

srs_score_desired_num_bins = 50
gamma = 0.1

spec = OrderedDict()

spec['area_type'] = {
    'num_classes'   : 2,
    'needs_binning' : False,
    'csv_index'     : 'Area type'
}

spec['intersect_channel'] = {
    'num_classes'   : 2,
    'needs_binning' : False,
    'csv_index'     : 'Intersection channelisation'
}

spec['curvature'] = {
    'num_classes'   : 4,
    'needs_binning' : False,
    'csv_index'     : 'Curvature'
}

spec['upgrade_cost'] = {
    'num_classes'   : 3,
    'needs_binning' : False,
    'csv_index'     : 'Upgrade cost'
}

spec['motorcycle_flow'] = {
    'num_classes'   : 6,
    'needs_binning' : False,
    'csv_index'     : 'Motorcycle observed flow'
}

spec['bicycle_flow'] = {
    'num_classes'   : 6,
    'needs_binning' : False,
    'csv_index'     : 'Bicycle observed flow'
}

spec['pedestrian_flow'] = {
    'num_classes'   : 6,
    'needs_binning' : False,
    'csv_index'     : 'Pedestrian observed flow across the road'
}

spec['drive_side_land_use'] = {
    'num_classes'   : 7,
    'needs_binning' : False,
    'csv_index'     : 'Land use - driver-side'
}

spec['passen_side_land_use'] = {
    'num_classes'   : 7,
    'needs_binning' : False,
    'csv_index'     : 'Land use - passenger-side'
}

# TODO Want speed limit, but data is a bit weird...
# spec['speed_limit'] = {
#     'num_classes'   : 7,
#     'needs_binning' : False,
#     'csv_index'     : 'Land use - passenger-side'
# }

spec['median_type'] = {
    'num_classes'   : 15,
    'needs_binning' : False,
    'csv_index'     : 'Median type'
}

spec['centre_rumble_strip'] = {
    'num_classes'   : 2,
    'needs_binning' : False,
    'csv_index'     : 'Centreline rumble strips'
}

spec['roadside_driver_side_distance'] = {
    'num_classes'   : 4,
    'needs_binning' : False,
    'csv_index'     : 'Roadside severity - driver-side distance'
}

spec['roadside_driver_side_object'] = {
    'num_classes'   : 17,
    'needs_binning' : False,
    'csv_index'     : 'Roadside severity - driver-side object'
}

spec['roadside_passenger_side_distance'] = {
    'num_classes'   : 4,
    'needs_binning' : False,
    'csv_index'     : 'Roadside severity - passenger-side distance'
}

spec['roadside_passenger_side_object'] = {
    'num_classes'   : 17,
    'needs_binning' : False,
    'csv_index'     : 'Roadside severity - passenger-side object'
}

spec['shoulder_rumble_strip'] = {
    'num_classes'   : 2,
    'needs_binning' : False,
    'csv_index'     : 'Shoulder rumble strips'
}

spec['driver_side_paved_shoulder'] = {
    'num_classes'   : 4,
    'needs_binning' : False,
    'csv_index'     : 'Paved shoulder - driver-side'
}

spec['passenger_side_paved_shoulder'] = {
    'num_classes'   : 4,
    'needs_binning' : False,
    'csv_index'     : 'Paved shoulder - passenger-side'
}

# TODO Code for intersection type is weird...
# spec['intersection_type'] = {
#     'num_classes'   : 4,
#     'needs_binning' : False,
#     'csv_index'     : 'Paved shoulder - passenger side'
# }

spec['intersection_road_volume'] = {
    'num_classes'   : 7,
    'needs_binning' : False,
    'csv_index'     : 'Intersecting road volume'
}

spec['intersection_quality'] = {
    'num_classes'   : 3,
    'needs_binning' : False,
    'csv_index'     : 'Intersection quality'
}

spec['num_lanes'] = {
    'num_classes'   : 6,
    'needs_binning' : False,
    'csv_index'     : 'Number of lanes'
}

spec['lane_width'] = {
    'num_classes'   : 3,
    'needs_binning' : False,
    'csv_index'     : 'Lane width'
}

spec['curve_quality'] = {
    'num_classes'   : 3,
    'needs_binning' : False,
    'csv_index'     : 'Quality of curve'
}

spec['road_condition'] = {
    'num_classes'   : 3,
    'needs_binning' : False,
    'csv_index'     : 'Road condition'
}

spec['vehicle_parking'] = {
    'num_classes'   : 3,
    'needs_binning' : False,
    'csv_index'     : 'Vehicle parking'
}

spec['passenger_side_sidewalk'] = {
    'num_classes'   : 7,
    'needs_binning' : False,
    'csv_index'     : 'Sidewalk - passenger-side'
}

spec['drivers_side_sidewalk'] = {
    'num_classes'   : 7,
    'needs_binning' : False,
    'csv_index'     : 'Sidewalk - driver-side'
}

spec['motorcycle_facilities'] = {
    'num_classes'   : 6,
    'needs_binning' : False,
    'csv_index'     : 'Facilities for motorised two wheelers'
}

spec['bicycle_facilities'] = {
    'num_classes'   : 7,
    'needs_binning' : False,
    'csv_index'     : 'Facilities for bicycles'
}

# Base labels
defaults = [['none'], ['none'], [.1], [1]]

# Any auxiliary labels
for key in spec.keys():
    if spec[key]['needs_binning']:
        defaults.append([.1])
        defaults.append([1])

    else:
        defaults.append([1])

def get_num_classes():
    with open('./dataset/num_bins.csv', 'r') as csvfile:
        num_classes = {}
        headers = csvfile.readline().strip().split(',')
        content = csvfile.readline().strip().split(',')

        for pair in zip(headers, content):
            num_classes[pair[0]] = int(pair[1])

    return num_classes
