import os
import util
import network
import dataset
import definition
import pandas as pd
import tensorflow as tf
import tensorflow.contrib.slim.nets

slim = tf.contrib.slim

spec = definition.spec

args = util.parse_arguments()
run_name = util.run_name(args)

# Google's inception v3 ckpt
inception_v3_ckpt = definition.inception_v3_ckpt

# Tawfiq's VGG16 Places 401 ckpt
places_ckpt = definition.places_ckpt

num_classes = definition.get_num_classes()

if args.restart:
    print('Removing ' + run_name + ' run from before')
    util.remove_run(run_name)

checkpoint_dir = definition.ckpt_base + run_name + '/'
log_dir = definition.log_base + run_name + '/'

if not tf.gfile.Exists(checkpoint_dir):
    print('Making checkpoint dir')
    os.makedirs(checkpoint_dir)

if not tf.gfile.Exists(log_dir):
    print('Making log dir')
    os.makedirs(log_dir)

with tf.variable_scope('Input'):
    print('Defining input pipeline')

    inputs, srs_bins, srs_score, other_labels, lat, lon = dataset.load_train()

with tf.variable_scope('Network'):
    print('Defining network')

    total_num_classes = num_classes['srs_score']

    # +1 for regression modeling
    if args.joint:
        print('Joint training regression and classification!')
        total_num_classes += 1

    for key in spec.keys():
        total_num_classes += num_classes[key]

    logits = network.network(inputs, total_num_classes)

    print('Network number of outputs: %d' % total_num_classes)

with tf.variable_scope('Loss'):
    print('Defining loss function')

    losses = {}
    
    # Regression is weighted 1x
    # Classification is weighted 10x
    # Everything else is weighted 1x

    # Classification loss
    srs_predicts = tf.slice(logits, [0,0], [-1, num_classes['srs_score']])
    loss_srs_class = tf.reduce_mean(
            tf.nn.sparse_softmax_cross_entropy_with_logits(srs_predicts,
                                                           srs_bins))
    losses['loss_srs_total_class'] = 10 * loss_srs_class

    # Get the losses from other variables
    computed = {}
    cursor = num_classes['srs_score']
    for key in spec.keys():
        computed[key] = tf.slice(logits, 
                    [0, cursor], [-1, num_classes[key]])
        cursor += num_classes[key]

    for idx, key in enumerate(spec.keys()):

        loss_class = tf.reduce_mean(
              tf.nn.sparse_softmax_cross_entropy_with_logits(computed[key],
                                         tf.to_int32(other_labels[:,idx])))
        losses['loss_' + key] = loss_class

    # Regression loss
    if args.joint:
        if not args.reg_loss_fn:
            print('Regression Loss fn not specified!')
            exit(0)

        reg_predicts = tf.squeeze(tf.slice(logits, [0, cursor], [-1, 1]))

        # Huber Loss
        if args.reg_loss_fn == 'huber':
            print('Using Huber loss for regression loss function!')
            g = lambda a: tf.cond(tf.abs(a) < 1, lambda: .5*tf.square(a),
                lambda: tf.abs(a)-.5)
            loss_srs_reg = tf.reduce_mean(
                    tf.map_fn(g, reg_predicts - srs_score))
        
        # L2 Loss
        if args.reg_loss_fn == 'l2':
            print('Using L2 loss for regression loss function!')
            loss_srs_reg = tf.reduce_mean(
                    tf.nn.l2_loss(reg_predicts - srs_score))

        losses['loss_srs_total_reg'] = loss_srs_reg

    # Compute Total Loss from all variables
    loss_classes = 0
    for key in losses.keys():
        loss_classes += losses[key]

    loss_reg = tf.add_n(tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES))

    loss = loss_classes + loss_reg

with tf.variable_scope('Train'):
    print('Defining optimizer')

    # Training only Mixed_7 block and last two layers
    model_vars = slim.get_model_variables()
    var_list = []

    for var in model_vars:
        if args.network == 'inception3':
            if 'Logits' in var.op.name:
                print(var.op.name)
                var_list.append(var)
        if args.network == 'vgg16':
            if 'ignore' in var.op.name:
                print(var.op.name)
                var_list.append(var)

    global_step = tf.Variable(0, name='global_step', trainable=False)
    learning_rate = tf.train.exponential_decay(args.lr,
                                               global_step,
                                               int(args.num_iter / 6),
                                               definition.gamma,
                                               staircase=True)
    optimizer = tf.train.AdamOptimizer(learning_rate, epsilon=0.1)
    train_op = optimizer.minimize(loss, 
                                  var_list=var_list,
                                  global_step=global_step)

    print('\nTraining for %d iterations' % args.num_iter)
    print('Learning rate: %f' % args.lr)
    print('Gamma: %f' % definition.gamma)
    print('Step size every %d\n' % int(args.num_iter / 6))

with tf.variable_scope('Summaries'):
    print('Defining summaries')

    for key in losses.keys():
        tf.summary.scalar(key, losses[key])

    tf.summary.scalar('loss_reg', loss_reg)
    tf.summary.scalar('loss', loss)
    tf.summary.scalar('learning_rate', learning_rate)

config = tf.ConfigProto()
config.gpu_options.allow_growth = True

# Make inception restorer
variables_to_restore = slim.get_model_variables()

if args.network == 'inception3':
    variables_to_restore = [ var for var in variables_to_restore
                                    if 'Logits' not in var.op.name ]
    variables_to_restore = { util.name_in_checkpoint(var, '') : var
                                for var in variables_to_restore }
if args.network == 'vgg16':
    variables_to_restore = [ var for var in variables_to_restore
                                    if 'ignore' not in var.op.name ]
    variables_to_restore = { util.name_in_checkpoint(var, 'vgg16/') : var
                                for var in variables_to_restore }

restorer = tf.train.Saver(variables_to_restore)

with tf.Session(config=config) as sess:
    print('Session started')

    update_ops = tf.group(*tf.get_collection(tf.GraphKeys.UPDATE_OPS))

    summary_writer = tf.summary.FileWriter(log_dir,
                                           sess.graph,
                                           flush_secs=5)
    summary = tf.summary.merge_all()

    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)
    sess.run(tf.global_variables_initializer())

    saver = tf.train.Saver()

    if not args.restart:
        ckpt = tf.train.get_checkpoint_state(checkpoint_dir)
        if ckpt and ckpt.model_checkpoint_path: 
            print('Restoring previous checkpoint')
            saver.restore(sess, ckpt.model_checkpoint_path)

    else:
        # Only restore from google checkpoint if train at full capacity
        if 1.0 - args.capacity < 0.000001:
            if args.network == 'inception3':
                print('Restoring Google Inception checkpoint')
                restorer.restore(sess, inception_v3_ckpt)
            if args.network == 'vgg16':
                print('Restoring Places VGG16 checkpoint')
                restorer.restore(sess, places_ckpt)

    _i = sess.run(global_step)

    log_file = log_dir + 'train_log.txt'

    print('Starting training')
    try:
        with open(log_file, 'a') as f:
            while _i < args.num_iter:
                _, _, _i, _loss, _summary, _lr = sess.run([
                    train_op,
                    update_ops,
                    global_step,
                    loss,
                    summary,
                    learning_rate])

                print('Step : %d | Loss : %.4f | LR : %f' % (_i, _loss, _lr))
                f.write('%06d | %.4f\n' % (_i, _loss))
                f.flush()

                summary_writer.add_summary(_summary, _i)
                summary_writer.flush()

                if _i % 500 == 0:
                    print('Saving checkpoint')
                    saver.save(sess, checkpoint_dir + 'model.ckpt',
                               global_step=_i)

    except KeyboardInterrupt:
        print('Saving checkpoint')
        saver.save(sess, checkpoint_dir + 'model.ckpt',
                   global_step=_i)

    finally:
        print('Cleaning up')
        coord.request_stop()
        coord.join(threads)
        print('Done')
