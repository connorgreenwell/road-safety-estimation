import os
import csv
import util
import dataset
import network
import definition

import numpy as np
import tensorflow as tf

spec = definition.spec

num_classes = definition.get_num_classes()

args = util.parse_arguments()
run_name = util.run_name(args)

checkpoint_dir = definition.ckpt_base + run_name + '/'
log_dir = definition.log_base + run_name + '/'

with tf.variable_scope('Input'):
    print('Defining input pipeline')

    inputs, ground_srs_bins, ground_srs_score, \
        ground_labels, lat, lon = dataset.load_test()
    
with tf.variable_scope('Network'):
    print('Defining network')

    total_num_classes = num_classes['srs_score']

    for key in spec.keys():
        total_num_classes += num_classes[key]

    # +1 for regression modeling
    if args.joint:
        total_num_classes += 1

    logits = network.network(inputs, total_num_classes)

config = tf.ConfigProto()
config.gpu_options.allow_growth = True

with tf.Session(config=config) as sess:
    print('Session started')

    sess.run(tf.global_variables_initializer())
    sess.run(tf.local_variables_initializer())
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)

    saver = tf.train.Saver()

    ckpt = tf.train.get_checkpoint_state(checkpoint_dir)
    if ckpt and ckpt.model_checkpoint_path: 
        print('Restoring previous checkpoint')
        saver.restore(sess, ckpt.model_checkpoint_path)
    
    # Setting up table to store errors, ground and prediction values
    # Latitude
    # Longitude
    # SRS Score Classification Bins
    # SRS prediction score
    # 29 x auxiliary labels (as of right now...)
    # Regression score, if joint trained
    num_columns = 4
    num_columns += len(spec.keys())

    if args.joint:
        num_columns += 1
        
    errors = [[] for x in range(num_columns)]
    grounds = [[] for x in range(num_columns)]
    predictions = [[] for x in range(num_columns)]
    batch_num = 1

    print('Evaluation started')
    try:
        while(True):
            g_srs_score,g_srs_bins,g_labels,_logits,_lat,_lon = sess.run([
                                                             ground_srs_score,
                                                             ground_srs_bins,
                                                             ground_labels,
                                                             logits,
                                                             lat,
                                                             lon])

            print('\n------Batch number %d------' % batch_num)

            errors[0].extend(_lat)
            errors[1].extend(_lon)
            grounds[0].extend(_lat)
            grounds[1].extend(_lon)
            predictions[0].extend(_lat)
            predictions[1].extend(_lon)
            
            grounds[2].extend(g_srs_bins)
            grounds[3].extend(g_srs_score)
            
            srs_logits = _logits[:, 0:num_classes['srs_score']]
            c_srs_bins = np.argmax(srs_logits, axis=1)
            c_srs_score = util.bin2srs(c_srs_bins)

            predictions[2].extend(c_srs_bins)
            predictions[3].extend(c_srs_score)

            srs_bin_error = np.absolute(c_srs_bins - g_srs_bins)
            srs_score_error = np.absolute(c_srs_score - g_srs_score)
            errors[2].extend(srs_bin_error)
            errors[3].extend(srs_score_error)

            print('Mean bin distance error: %.4f' % np.mean(srs_bin_error))
            print('Mean score error: %.4f' % np.mean(srs_score_error))

            cursor = num_classes['srs_score']
            for idx, key in enumerate(spec.keys()):
                c_label = _logits[:, cursor:cursor+num_classes[key]]
                c_label = np.squeeze(np.argmax(c_label, axis=1))
                g_label = np.squeeze(g_labels[:, idx])

                # Check to see if this is a single element batch
                if c_label.size == 1:
                    c_label = c_label.reshape((1,))
                    g_label = g_label.reshape((1,))
                
                label_error = np.absolute(c_label - g_label)

                errors[4+idx].extend(label_error)
                grounds[4+idx].extend(g_label)
                predictions[4+idx].extend(c_label)

                num_wrong_predicts = np.count_nonzero(label_error)
                print('Wrong %s predicts: %d' % (key, num_wrong_predicts))
                cursor += num_classes[key]

            if args.joint:
                c_srs_reg = np.squeeze(_logits[:, cursor:])

                if c_srs_reg.size == 1:
                    c_srs_reg = c_srs_reg.reshape((1,))
                    g_srs_score = g_srs_score.reshape((1,))

                reg_error = np.absolute(c_srs_reg - g_srs_score)
                    
                errors[-1].extend(reg_error)
                grounds[-1].extend(g_srs_score)
                predictions[-1].extend(c_srs_reg)
                print('Mean regression error: %.4f' % np.mean(reg_error))

            batch_num += 1

    except tf.errors.OutOfRangeError:
        print('Queue empty, saving errors, predictions and grounds...')

        headers = []
        headers.append('latitude')
        headers.append('longitude')
        headers.append('srs_bins')
        headers.append('srs_score')
        headers.extend(spec.keys())
        if args.joint: headers.append('srs_reg')

        errors_file = checkpoint_dir + 'errors.csv'
        grounds_file = checkpoint_dir + 'grounds.csv'
        predicts_file = checkpoint_dir + 'predicts.csv'

        errors = zip(*errors)
        grounds = zip(*grounds)
        predictions = zip(*predictions)

        with open(errors_file, 'w') as f:
            csvwriter = csv.writer(f, delimiter=',')
            csvwriter.writerow(headers)
            for line in errors:
                csvwriter.writerow(line)
        with open(grounds_file, 'w') as f:
            csvwriter = csv.writer(f, delimiter=',')
            csvwriter.writerow(headers)
            for line in grounds:
                csvwriter.writerow(line)
        with open(predicts_file, 'w') as f:
            csvwriter = csv.writer(f, delimiter=',')
            csvwriter.writerow(headers)
            for line in predictions:
                csvwriter.writerow(line)

    finally:
        coord.request_stop()
        coord.join(threads)
