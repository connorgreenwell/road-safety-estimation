import os
import time
import shutil
import argparse
import numpy as np
import tensorflow as tf
import cPickle as pickle

# Helper function to remove "Network" prefix 
import scipy.io as sio

def name_in_checkpoint(var, replacement):
    if "Network" in var.op.name:
        return var.op.name.replace("Network/", replacement)

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', dest='csv_file', action='store',
            type=str, default='')
    parser.add_argument('-c', dest='capacity', type=float, default=1.0)

    parser.add_argument('--gpu01', dest='gpu01', 
            action='store_true', default=False)

    parser.add_argument('--restart', dest='restart',
            action='store_true', default=False)

    parser.add_argument('-j', '--joint', dest='joint',
            action='store_true', default=False)

    parser.add_argument('-r', '--reg_loss_fn', dest='reg_loss_fn',
            action='store', type=str, default='l2')

    parser.add_argument('-n', '--network', dest='network',
            action='store', type=str, default='inception3')

    parser.add_argument('-i', '--input', dest='input',
            action='store', type=str, default='ground')

    parser.add_argument('--num_iter', dest='num_iter',
            action='store', type=int, default='300000')

    parser.add_argument('--lr', dest='lr',
            action='store', type=float, default='0.001')
    
    args = parser.parse_args()

    return args

def run_name(args):
    run_name = ''
    run_name += args.network + '_'
    run_name += str(args.capacity) + '_'
    run_name += str(args.num_iter) + '_'
    run_name += str(args.lr) + '_'
    run_name += args.input + '_'

    if(args.joint):
        run_name += 'joint_' + args.reg_loss_fn
    else:
        run_name += 'no-joint'

    return run_name

def remove_run(run_name):
    if os.path.exists('./checkpoint/' + run_name):
        shutil.rmtree('./checkpoint/' + run_name)
    if os.path.exists('./logs/' + run_name):
        shutil.rmtree('./logs/' + run_name)

def evalBin(queries, bin_edges):
    results = []

    for query in queries:
        if query < 0 or query > len(bin_edges):
            raise Exception('Bin position not possible')
        elif query == 0:
            results.append(bin_edges[1])
        elif query == len(bin_edges)-2:
            results.append(bin_edges[len(bin_edges)-2])
        else:
            results.append((bin_edges[query] + bin_edges[query+1]) / 2)

    return np.array(results)

def bin2srs(srs_bins):
    srs_edges = pickle.load(open('./dataset/srs_score_bins.pkl', 'r'))

    return evalBin(srs_bins, srs_edges)

def dist2srs(srs_dist):
    srs_bins = [ x.argmax() for x in srs_dist ]

    return bin2srs(srs_bins)
