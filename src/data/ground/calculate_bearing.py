import os
import glob
import numpy as np
import pandas as pd
import xml.etree.ElementTree as ET

model_dir = '../../model/'

import sys
sys.path.append(model_dir)

import definition

img_base = definition.img_base
csv_base = definition.raw_csv_base

if __name__ == '__main__':
    loc_list = glob.glob(csv_base + '*.csv')

    bearings = []

    for csv_file in loc_list:
        df = pd.read_csv(csv_file, dtype=str)

        previous_theta = 0.0

        for idx in range(len(df)-1):

            lat1, lon1 = df['Latitude'][idx], df['Longitude'][idx]
            lat2, lon2 = df['Latitude'][idx+1], df['Longitude'][idx+1]

            # If we do not have panorama for the location, skip
            if not os.path.exists(img_base + '%s,%s/pano.jpg' % (lat1, lon1)):
                continue

            current_road = df['Road name'][idx]
            next_road = df['Road name'][idx+1]

            # If we are on the same road, calculate bearing
            if current_road == next_road:
                lat1_f = float(lat1)
                lon1_f = float(lon1)
                lat2_f = float(lat2)
                lon2_f = float(lon2)

                lon_diff = lon1_f- lon2_f

                x1 = np.sin(lon_diff) * np.cos(lat2_f)
                x2 = np.cos(lat1_f) * np.sin(lat2_f) \
                     - np.sin(lat1_f) * np.cos(lat2_f) * np.cos(lon_diff)
                theta = (np.degrees(np.arctan2(x1, x2)) + 360) % 360

                bearings.append((lat1, lon1, theta))
                previous_theta = theta

            # If not the same road, use previously calculated theta
            else:
                bearings.append((lat1, lon1, previous_theta))

    bearings_df = pd.DataFrame(data=bearings)
    bearings_df.to_csv('bearings.csv', index=False, header=False)
