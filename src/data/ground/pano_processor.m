addpath('./helper_scripts/');

outpath = '/u/vul-d1/scratch/wso226/road-safety-estimation/images';

fid = fopen('./bearings.csv', 'r');
bearings = textscan(fid, '%s %s %f', 'Delimiter', ',');

parfor i = 1:length(bearings{1})
    lat = bearings{1}{i};
    lon = bearings{2}{i};
    bearing = bearings{3}(i);
    
    pano_file = sprintf('%s/%s,%s/pano_calibrated.jpg', outpath, lat, lon);
    out_file = sprintf('%s/%s,%s/pano_cut.jpg', outpath, lat, lon);
    
    generate_cutout(out_file, pano_file, [299 299], 360-bearing, 0, 0, 120);
    
    fprintf([out_file '\n'])
end