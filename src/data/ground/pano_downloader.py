import glob
import numpy as np
import pandas as pd

streetviewprocessing_dir = '/mounts/u-amo-d0/ugrad/wso226/MachineLearning/BitBucket/streetviewprocessing/src/'
model_dir = '../../model/'

import sys
sys.path.append(model_dir)

import streetview
import definition

img_base = definition.img_base
csv_base = definition.raw_csv_base

def ensure_dir(filename):
  if not os.path.exists(os.path.dirname(filename)):
    try:
      os.makedirs(os.path.dirname(filename))
    except OSError, e:
      if e.errno != errno.EEXIST:
        raise


if __name__ == '__main__':
    loc_list = glob.glob(csv_base + '*.csv')

    for loc in loc_list: print(loc)

    all_coords = []

    for csv_file in loc_list:
        df = pd.read_csv(csv_file, dtype=str)
        all_coords.extend(zip(list(df['Latitude']), list(df['Longitude'])))

    downloader = streetview.StreetViewDownloader(img_base, num_decimals=-1)
    missing_coords = downloader.grabber(all_coords)

    with open('./missing_coords.csv', 'w') as f:
        for pair in missing_coords:
            f.write('%s,%s\n' % (pair[0], pair[1]))
