function generate_cutout(outFile, filename, sz, yaw, pitch, roll, fov)


im = imread(filename);


%% map output image to input image

% Perspective view parameters
iimh = 3328/2;  iimw = 6656/2;  % input  image size
oimh = sz(1);  oimw = sz(2);           % output image size
hfov = fov/180*pi;              % horizontal filed of view (radians)

f = oimw/(2*tan(hfov/2));           % focal length (pixels)
ouc = (oimw+1)/2; ovc = (oimh+1)/2; % output image center
iuc = (iimw+1)/2; ivc = (iimh+1)/2; % input image center


%% tangent plane to unit sphere mapping

[X, Y] = meshgrid(1:oimw, 1:oimh);
X = X-ouc;   Y = Y-ovc;           % shift origin to the image center
Z = repmat(f, size(X));
PTS = [X(:)'; Y(:)'; Z(:)'];

% rotation w.r.t x-axis about pitch angle
Tpitch = [1 0 0; ...
    0 cos(pitch/180*pi) -sin(pitch/180*pi);...
    0 sin(pitch/180*pi) cos(pitch/180*pi);];

Tyaw = [cos(yaw/180*pi) 0 sin(yaw/180*pi); ...
    0 1 0;...
    -sin(yaw/180*pi) 0 cos(yaw/180*pi);];

Troll = [cos(roll/180*pi) -sin(roll/180*pi) 0; ...
    sin(roll/180*pi) cos(roll/180*pi) 0;...
    0 0 1;];

PTSt = Tyaw*Troll*Tpitch*PTS;

% cartesian to spherical
Xt = reshape(PTSt(1,:),oimh, oimw);
Yt = reshape(PTSt(2,:),oimh, oimw);
Zt = reshape(PTSt(3,:),oimh, oimw);

% mapping from unit sphere grid to cylinder
sw = iimw/2/pi;
sh = iimh/pi;

theta = atan2(Xt, Zt);
phi = atan(Yt./sqrt(Xt.^2+Zt.^2));

U = sw*theta + iuc;
V = sh*phi + ivc;
oim = iminterpnn(im, U, V);

imwrite(oim, outFile);
