''' streetview: Implements Google StreetView static api.
'''
import os, csv
import cStringIO 
from br import br
import numpy as np
from PIL import Image
from lxml import etree
from multiprocess import Pool
from random import shuffle, randint, uniform

def ensure_dir(filename):
  if not os.path.exists(os.path.dirname(filename)):
    try:
      os.makedirs(os.path.dirname(filename))
    except OSError, e:
      if e.errno != errno.EEXIST:
        raise

def truncate(f, n):
  '''Truncates/pads a float f to n decimal places without rounding'''
  s = '{}'.format(f)
  if 'e' in s or 'E' in s:
    return '{0:.{1}f}'.format(f, n)
  i, p, d = s.partition('.')
  base = '.' if n > 0 else ''
  return base.join([i, (d+'0'*n)[:n]])


class StreetViewDownloader:

  def __init__(self, outdir, num_decimals=0):
    self.xml_dir = outdir
    self.img_dir = outdir
    self.num_decimals = num_decimals

    if not os.path.exists(self.xml_dir):
      os.makedirs(self.xml_dir)
    if not os.path.exists(self.img_dir):
      os.makedirs(self.img_dir)

  def get_xml(self, lat, lon):
    serv = 'http://cbk%d.google.com/' % randint(0,3)
    info_url = serv + 'cbk?output=xml&ll=%s,%s'
    
    try:
      xml = br.open(info_url % (lat, lon)).read()
    except Exception as e:
      xml = None
    return xml    

  def get_metadata(self, tree):
    if len(tree):
      lat = tree[0].get('lat')
      lon = tree[0].get('lng')
      pano_id = tree[0].get('pano_id')
      return float(lat), float(lon), pano_id

  def get_thumbnail(self, fname, width, height, pano_id):
    thumb_url = 'http://cbk0.google.com/cbk?output=thumbnail&w=%d&h=%d&panoid=%s'
    return br.retrieve(thumb_url % (width, height, pano_id), fname)

  def get_tile(self, tile_url):
    try:
      raw_data = br.open(tile_url).read()
      memory_file = cStringIO.StringIO(raw_data)
      tile = np.array(Image.open(memory_file))
    except Exception as e:
      print "Tile error: %s" % tile_url
      print e
      tile = np.array([]) 
    return tile

  def get_panorama(self, fname, pano_id, zoom_level=3):
    server_url = 'http://cbk%d.google.com/' % randint(0,3)
    pano_url = server_url + 'cbk?output=tile&panoid=%s&zoom=%d&x=%d&y=%d'
    zoom_sizes = {3:(7,4), 4:(13,7), 5:(26,13)}
    max_x, max_y = zoom_sizes[zoom_level]
  
    jobs = []
    for y in xrange(max_y):
      for x in xrange(max_x):
        tile_url = pano_url % (pano_id, zoom_level, x, y)
        jobs.append(tile_url)
    
    p = Pool(len(jobs))
    tiles = p.map(self.get_tile, jobs)
    p.close()

    if all(x.size for x in tiles):
      tiles = np.array(tiles)
      strips = []
      for y in xrange(max_y):
        strips.append(np.hstack(tiles[y*max_x:(y+1)*max_x,:,:,:]))
      pano = np.vstack(strips)
      pano = pano[0:1664, 0:3328]
    else:
      pano = np.array([])
    return pano


  def grabber(self, input):
    if type(input) is str:
      csv_fname = open(input, 'r')
      locs = list(csv.reader(csv_fname))
    elif type(input) is list:
      locs = input
    else:
      raise Exception("neither file of, nor list of, geo-coordinates provided")

    print "processing %d locations" % len(locs)
    shuffle(locs)

    missing_coords = []

    with open('available_coords.csv', 'w') as fb:
      for q_lat, q_lon in locs:
        xml = self.get_xml(q_lat, q_lon)
        if xml:
          tree = etree.XML(xml)
          if len(tree):
            [lat, lon, pano_id] = self.get_metadata(tree)

            # Don't truncate if told not to
            if self.num_decimals >= 0:
              lat_bin = truncate(lat, self.num_decimals)
              lon_bin = truncate(lon, self.num_decimals)
            else:
              lat_bin = lat
              lon_bin = lon
            
            print "pano found: %s,%s (%s)" % (q_lat, q_lon, pano_id)
            base_fname = '%s_%s' % (q_lat, q_lon)
            
            # xml_fname = os.path.join(self.xml_dir, lat_bin, lon_bin, base_fname + '.xml')
            xml_fname = self.xml_dir + '%s,%s/pano.xml' % (q_lat, q_lon)
            if not os.path.exists(os.path.dirname(xml_fname)):
              ensure_dir(xml_fname)
            f = open(xml_fname, 'w')
            f.write(xml)
            f.close()

            # pano_fname = os.path.join(self.img_dir, lat_bin, lon_bin, base_fname + '.jpg')
            pano_fname = self.img_dir + '%s,%s/pano.jpg' % (q_lat, q_lon)
            if not os.path.exists(os.path.dirname(pano_fname)):
              ensure_dir(pano_fname)
            pano = self.get_panorama(base_fname, pano_id)
            if pano.size:
              Image.fromarray(pano).save(pano_fname)

            fb.write(self.img_dir + '%s,%s\n' % (q_lat, q_lon))
            fb.flush()

          else:
            print "pano not available: %s,%s" % (q_lat, q_lon)
            missing_coords.append((q_lat, q_lon))

    return missing_coords

  def sample_from_bounding_box(self, sample_size, lat_min, lat_max, lon_min, lon_max, boundary=None):
    coords = []
    for _ in range(sample_size):
      lat = uniform(lat_min, lat_max)
      lon = uniform(lon_min, lon_max)
      if boundary:
        if boundary.contains_point(np.asarray([lat, lon])):
          coords.append((lat, lon))
      else:
        coords.append((lat, lon))
    
    self.grabber(coords)

if __name__ == "__main__":
  
    gg = StreetViewDownloader('./test/', num_decimals=2)
    #gg.grabber('locations.csv')
    #gg.grabber([(-81.387420999979156, 30.241079000395530), (-81.380767999843329, 30.195950000292271)])
    #gg.sample_from_bounding_box(100000, 36.873693067455, 38.6647053878363, -123.54834121703, -121.262911966621)
    
    # boundary example
    import scipy.io
    from matplotlib import path
    bbox = np.asarray([28.3609478187, -81.5697097778, 28.384354005, -81.537437439])
    lat = bbox[[0,2,2,0,0]]
    lon = bbox[[1,1,3,3,1]]
    boundary = path.Path(np.vstack((lat, lon)).T)
    
    gg.sample_from_bounding_box(100, np.min(lat), np.max(lat), np.min(lon), np.max(lon), boundary=boundary)
