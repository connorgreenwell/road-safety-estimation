import sys

sys.path.append('../../../model/')

import definition

f = open('../available_coords.csv', 'r')
f_lines = f.readlines()
f.close()

img_base = definition.img_base

with open('../panos.txt', 'w') as f:
    for line in f_lines:
        f.write(img_base + '%s/\n' % line.strip())       
