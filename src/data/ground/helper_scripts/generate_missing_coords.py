import os
import glob
import numpy as np
import pandas as pd

model_dir = '../../../model/'

import sys
sys.path.append(model_dir)

import definition

img_base = definition.img_base
csv_base = definition.raw_csv_base

if __name__ == '__main__':
  loc_list = glob.glob(csv_base + '*.csv')

  with open('../missing_coords.csv', 'w') as f:
    for csv_file in loc_list:
      df = pd.read_csv(csv_file, dtype=str)

      for idx in range(len(df)-1):

        lat, lon = df['Latitude'][idx], df['Longitude'][idx]

        # If we do not have panorama for the location, skip
        if not os.path.exists(img_base + '%s,%s/pano.jpg' % (lat, lon)):
          f.write('%s,%s\n' % (lat, lon))
