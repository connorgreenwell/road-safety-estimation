import glob
import pandas as pd
from PIL import Image

import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt

model_dir = '../../model/'

import sys
sys.path.append(model_dir)

import definition

img_base = definition.img_base
csv_base = definition.raw_csv_base

if __name__ == '__main__':
    loc_list = glob.glob(csv_base + '*.csv')
    
    for csv_file in loc_list:
        df = pd.read_csv(csv_file, dtype=str)

        for idx in range(len(df)-1):

            lat, lon = df['Latitude'][idx], df['Longitude'][idx]

            img_path = img_base + '%s,%s/pano_cut.jpg' % (lat, lon)

            plt.imshow(Image.open(img_path))
            plt.pause(0.25)
            plt.clf()
