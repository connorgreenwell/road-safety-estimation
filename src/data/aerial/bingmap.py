"""
    Script used to download Bing satellite images

    Author: Menghua Zhai
"""
import os
import errno
import urllib
import csv
import glob
import pandas as pd
from multiprocessing import Pool

import sys
sys.path.append('../../model/')

import definition

img_base = definition.img_base
csv_base = definition.raw_csv_base

def ensure_dir(filename):
  if not os.path.exists(os.path.dirname(filename)):
    try:
      os.makedirs(os.path.dirname(filename))
    except OSError, e:
      if e.errno != errno.EEXIST:
        raise

def download(item):
  url = item[0]
  fn = item[1] 
  
  print url, fn
  ensure_dir(fn)
  try:
    urllib.urlretrieve(url, fn)
  except:
    print "failed: %s" % (url)

if __name__ == '__main__':

  loc_list = glob.glob(csv_base + '*.csv')
  
  data = []

  for csv_file in loc_list:
    df = pd.read_csv(csv_file, dtype=str)
    data.extend(zip(df['Latitude'], df['Longitude']))

  # settings
  zoom_level = 19
  im_size = "325,325"
  api_key = "AvKZgqJskqXkT564_y9CI3TdFyJ7c2ppnEnTtkCiKmr_qmQjZ9tenlGeINECCgJT"

  template_url = "https://dev.virtualearth.net/REST/v1/Imagery/Map/Aerial/%%s/%d?mapSize=%s&key=%s" % (zoom_level, im_size, api_key)

  jobs = []
  for item in data:
    lat, lon = item[0], item[1]
    query_loc = "%s,%s" % (lat, lon)
    image_url = template_url % (query_loc)
    out_file = img_base + "%s,%s/satellite.jpg" % (lat, lon)
    jobs.append([image_url, out_file])

  p = Pool(60)
  p.map(download, jobs)
