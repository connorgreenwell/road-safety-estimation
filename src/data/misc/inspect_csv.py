import csv
import glob
import sys
sys.path.append('../')
import util
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt

args = util.parse_arguments()

f = open(args.csv_file)
f_headers = f.readline().strip().split(',')
f_lines = f.readlines()
f.close()

content = {}
for key in f_headers:
    content[key] = []

for line in f_lines:
    tokens = line.strip().split(',')
    for (key, token) in zip(f_headers, tokens):
        content[key].append(token)

key = ''

while(True):
    key = input('\nKey Index (-1 quit, -2 list keys): ')

    if key == -1: exit(0)

    if key == -2:
        for idx, header in enumerate(f_headers):
            print('%3d | %s' % (idx, header))
        print('\n\n')
        continue

    how = input('Method of Visualization:\n 1) Histogram\n 2) Raw\n---> ')

    if how == 1:
        num_bins = input('Number of bins: ')
        data = np.array(content[f_headers[key]])
        data = data.astype(np.float32)
        plt.hist(data, bins=num_bins)
        plt.show()

    if how == 2:
        for x in content[f_headers[key]]:
            print x
